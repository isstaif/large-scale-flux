var _ = require('underscore')

//core plugins
var AuthPlugin = require('./plugins/auth')
var I18nPlugin = require('./plugins/i18n')
var DispatcherPlugin = require('./plugins/dispatcher')
var ChildModulePlugin = require('./plugins/child')

//module extensions
var ReactExtension = require('./extensions/react')
var TokenManagerExtension = require('./extensions/tokens-manager')
var ContainerExtension = require('./extensions/container')

var globalModules = {}

var Core = function(options, CustomSandbox){

	options = options || {}

	//private scope
	var modulesIndex = {}
	var instancesIndex = {}
	var plugins = []
	var extensions = []

	var Sandbox = this.Sandbox = CustomSandbox || require('./sandbox')
	var core = this

	var parentSandbox = options.parentSandbox || null

	//core methods
	this.register = function(name, module){
		modulesIndex[name] = module
	}

	this.getInstance = function(instanceId){
		return instancesIndex[instanceId]
	}

	this.getChildren = function(){
		var children = []
		for (var instanceId in instancesIndex){			
			children.push(instancesIndex[instanceId])
		}
		return children		
	}

	this.use = function(Plugin){
		plugins.push(Plugin)
		Plugin.call(this, options)
	}

	this.extension = function(Extension){
		extensions.push(Extension)
	}

	this.getGlobalModule = function(instanceId){
		if (typeof globalModules[instanceId] == 'undefined')
			return false

		return globalModules[instanceId]
	}

	this.instantiate = function(moduleName, options){

		options = options || {}

		var instanceId = options.instanceId
		
		//creating the sandbox
		var sandbox  = new Sandbox(this, instanceId, options.options, moduleName)

		sandbox.parentSandbox = parentSandbox

		ContainerExtension(sandbox, core, options)
		
		//creating an instance of the module
		var ModuleClass = modulesIndex[moduleName]
		if (!ModuleClass) 
			throw Error("Core: no registered module: " + moduleName)
		var instance = new ModuleClass(sandbox)

		if (!options.global){
			//registering the created instance
			instancesIndex[instanceId] = instance
		} else {
			globalModules[instanceId] = instance
		}

		instance.getInstanceId = function(){
			return instanceId
		}

		instance.getModuleId = function(){
			return moduleName
		}

		//running extensions
		extensions.forEach(function(Extension){
			Extension(instance, sandbox, core, options)
		})	
		
		return instance
	}

	//default plugins
	this.use(AuthPlugin)
	this.use(I18nPlugin)
	this.use(DispatcherPlugin)
	this.use(ChildModulePlugin)	

	//default extensions
	this.extension(ReactExtension)
	this.extension(TokenManagerExtension)

	return this
}

module.exports = Core