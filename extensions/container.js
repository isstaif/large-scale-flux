
function ContainerExtension(sandbox, core, options){

	if (options.isApp){
		sandbox.getCurrentContainer = function(){
			return options.options.data.container
		}
		sandbox.getCurrentMembership = function(){
			return options.options.data.membership
		}

	} else {
		sandbox.getCurrentContainer = function(){
			if (sandbox.parentSandbox){
				return sandbox.parentSandbox.getCurrentContainer()
			} 
		}
		sandbox.getCurrentMembership = function(){
			if (sandbox.parentSandbox){
				return sandbox.parentSandbox.getCurrentMembership()
			} 
		}

	}

}

module.exports = ContainerExtension