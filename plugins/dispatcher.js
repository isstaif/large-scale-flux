var Dispatcher = require('flux').Dispatcher 
	_ = require('underscore')

function DispatcherPlugin (options){

	//dispatcher	
	var dispatcher = options.dispatcher
	var tokensIndex = {}

	//creating a new dispatcher in the root core
	if (!dispatcher){
		dispatcher = new Dispatcher()
	}

	this.sleepTokens = function (instanceId){
		var tokenRecords = tokensIndex[instanceId]
		if (tokenRecords){
			tokenRecords.forEach(function(tokenRecord){
				//console.log('sleeping', tokenRecord.token, instanceId)
				dispatcher.unregister(tokenRecord.token)
			})			
		}
	}

	this.wakeTokens = function (instanceId){
		var tokenRecords = tokensIndex[instanceId]
		if (tokenRecords){
			tokenRecords.forEach(function(tokenRecord){
				var newToken = dispatcher.register(tokenRecord.callback)
				//console.log('waking up', tokenRecord.token, instanceId, 'with new token', newToken)
				tokenRecord.token = newToken
			})			
		}
	}

	this.registerCallback = function(callback, instanceId){
		var token = dispatcher.register(callback)

		var tokenRecord = { token : token, callback : callback }
		if (!tokensIndex[instanceId]) tokensIndex[instanceId] = []
		tokensIndex[instanceId].push(tokenRecord)
		//console.log('registering', token, instanceId)
		return token
	}	

	this.dispatch = function(action){
		dispatcher.dispatch(action)
	}

	this.getDispatcher = function(){
		return dispatcher
	}

	this.getTokens = function(){
		return tokensIndex
	}

	//helper methods for the sandbox
	var core = this
	_.extend(this.Sandbox.prototype,{
		handleAction : function(action){
			var instanceId = this.id
			action.moduleInstance = instanceId
			core.dispatch(action)
		},
		listen : function(callback){
			var instanceId = this.id			
			var token = core.registerCallback(callback, instanceId)
			return token
		},	
	})
}

module.exports = DispatcherPlugin