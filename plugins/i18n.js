//var munityMessages = require('munity/messages')

function I18nPlugin (options){

	//internationalization
	var messages
	if (options.messagesObject){
		messages = options.messagesObject
	} else if (options.messages){
		messages = options.messages
	}
	this.messages = messages

	this.Sandbox.prototype.messages = messages

}

module.exports = I18nPlugin