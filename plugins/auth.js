var _ = require('underscore')

function AuthPlugin(options){

	//login helpers
	var currentUser = options.currentUser
	var currentContainer
	var currentMembership

	this.getCurrentUser = function(){
		return currentUser
	}	
	this.isLoggedIn = function(){
		if (this.getCurrentUser())
			return true
		else 
			return false
	}

	this.setCurrentContainer = function(container){
		return currentContainer = container
	}
	this.setCurrentMembership = function(membership){
		return currentMembership = embership
	}
	this.getCurrentContainer = function(){
		return currentContainer
	}
	this.getCurrentMembership = function(){
		return currentMembership
	}

	var core = this

	_.extend(this.Sandbox.prototype, {
		getCurrentUser : function(){
			return core.getCurrentUser()
		},
		isLoggedIn : function(){
			return core.isLoggedIn()
		},
		setCurrentContainer : function(container){
			return setCurrentContainer(container)
		},
		setCurrentMembership : function(membership){
			return setCurrentMembership(membership)
		},
		getCurrentContainer : function(){
			return getCurrentContainer()
		},
		getCurrentMembership : function(){
			return getCurrentMembership()
		},
	})
}

module.exports = AuthPlugin