var _ = require('underscore')

function ChildModulePlugin(options){

	var Core = require('./../core')

	var core = this


	//each modules has a sub core, to create independent sub modules
	this.Sandbox.prototype.SubCore = function(options){
		
		return new Core({ 
			currentUser : core.getCurrentUser(),
			dispatcher : core.getDispatcher(),
			messages : core.messages,
			parentSandbox : options.parentSandbox
		})	
	} 

}

module.exports = ChildModulePlugin